<?php
/*
	routes.php
	Crie url personalizadas atraves da configuracao abaixo

	name: Nome da rota
	controller: Nome do controler a ser usado
	Action: Nome do metodo a ser chamado pelo controller
*/

$_ROUTERS = [
	
	
	['name'=>'nova-conta', 'controller'=>'usuarios', 'action'=>'nova_conta'],
	['name'=>'criar-conta', 'controller'=>'usuarios', 'action'=>'criar_conta'],
	['name'=>'recuperar-conta', 'controller'=>'usuarios', 'action'=>'recuperar_conta'],
	['name'=>'atualizar-conta', 'controller'=>'usuarios', 'action'=>'atualizar_conta'],
	['name'=>'logar', 'controller'=>'usuarios', 'action'=>'logar'],
	['name'=>'meus-dados', 'controller'=>'dashboard', 'action'=>'meus_dados'],
	['name'=>'create-template', 'controller'=>'dashboard', 'action'=>'create_template'],
	['name'=>'save-template', 'controller'=>'dashboard', 'action'=>'save_template'],
	['name'=>'delete-template', 'controller'=>'dashboard', 'action'=>'delete_template'],
	['name'=>'importar-arquivo', 'controller'=>'dashboard', 'action'=>'importar_arquivos'],
	['name'=>'salvar-arquivo', 'controller'=>'dashboard', 'action'=>'salvar_arquivo'],
	['name'=>'delete-arquivo', 'controller'=>'dashboard', 'action'=>'delete_arquivo'],
	['name'=>'configurar-arquivo', 'controller'=>'dashboard', 'action'=>'configurar_arquivo'],
	['name'=>'salvar-configuracao', 'controller'=>'dashboard', 'action'=>'salvar_configuracao'],
	['name'=>'list-templates', 'controller'=>'dashboard', 'action'=>'list_templates'],
	['name'=>'enviar-imagens', 'controller'=>'dashboard', 'action'=>'upload_image'],
	['name'=>'salvar-imagens', 'controller'=>'dashboard', 'action'=>'save_image'],
	['name'=>'delete-imagem', 'controller'=>'dashboard', 'action'=>'delete_image'],
	['name'=>'configurar-db', 'controller'=>'init', 'action'=>'config_db'],
	['name'=>'criar-db', 'controller'=>'init', 'action'=>'create_db'],
	['name'=>'atualizar-arquivo', 'controller'=>'dashboard', 'action'=>'atualizar_arquivos']
];