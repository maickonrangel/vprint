<?php

// ambiente
if (isset($_SERVER['HTTP_HOST']) && substr_count($_SERVER['HTTP_HOST'], '127.0.0.1')) {
	$_ENVIRONMENT 	= 'local';
	$_DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	$_SERVER_NAME 	= $_SERVER['SERVER_NAME'];

} elseif (isset($_SERVER['HTTP_HOST']) && substr_count($_SERVER['HTTP_HOST'], 'uniqsuporte.com.br')) {
	$_ENVIRONMENT = 'production';
	$_DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	$_SERVER_NAME 	= $_SERVER['SERVER_NAME'];
} else {
	$_ENVIRONMENT 	= 'local';
	$_DOCUMENT_ROOT = 'C:/xampp/htdocs';
	$_SERVER_NAME 	= 'http://127.0.0.1/';
}

// Configuracao
$_CONFIG = [

	// aplicacao local
	'app' => [
		// local
		'local' => [
			'project_dir' 	=> $_DOCUMENT_ROOT,
			'file_path' 	=> '/vprints/',
			'url_path'		=> $_SERVER_NAME
			],
		
		// producao
		'production' => [
			'project_dir' 	=> $_DOCUMENT_ROOT,
			'file_path' 	=> '/home/amindcom/public_html/vprint/',
			'url_path'		=> $_SERVER_NAME
			],
	],
	
	// banco de dados
	'db' => [
		// local
		'local' => [
			'db_name' 		=> 'vprint',
			'db_host' 		=> '127.0.0.1',
			'db_user' 		=> 'root',
			'db_pass' 		=> '',
		],

		// producao
		'production' => [
			'db_name' 		=> 'amindcom_vprint',
			'db_host' 		=> 'localhost',
			'db_user' 		=> 'amindcom_vprint',
			'db_pass' 		=> 'N7J}%Xxic;[u',
		]
	],

	'lenguage' => [
		// local
		'local' => 'pt-br',
		// producao
		'production' => 'pt-br'
	]
];