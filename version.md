    Vprint v0.6

Vprint é um sistema para gerenciamento e criação de cartazes desenvolvido por Maickon rangel.
A duplicação e distribuição deste software sem a permissão do desenvolvedor é proibida, bem como apropriação para comércio com terceiros ou meios relacionados.   

Últimas Alterações
==================
#### 13/04/2018 [v0.1]
* Carregar os nomes na caixa de configuração de cada elemento do cartaz ✅
* Permitir a opção de repetir um produto no mesmo cartaz ✅
* Permitir que produto apareça de forma repetida no filtro após pesquisa-lo mais de uma vez [Meio fora de mão] ❌
* Criar cartazes de tamanho A1, A2 e A3 com orientação paisagem e retrato ] ❌
* Permitir que o tamanho da fonte se auto ajuste com o espaço disponível] ❌

#### 24/04/2018 [v0.2]
* Editar cartaz indivisualmente ✅
* Ajustar a quantidade de nomes por cada linha no cartaz ✅
* Colocar um botão de atualizar cartaz ou invés de atualizar automático ✅
* Adicionar a opção de poder remover qualquer elemento do cartaz ✅
* Todos os campos já virem com XXX preenchidos [Não necessário mais] ❌

#### 08/05/2018 [v0.3]
* Fazer o texto de cartaz sempre ficar dentro do quadrado vermelho ❌
* Corrigir a opção de cartazes combinados ✅
* Adicionar opção para editar o nome do cartaz ✅
* Adicionar texto adicional para descrever (de, por, cada, kg, unidade e etc) de forma automática ✅
* Fazer com que o redimensionamento não afete outros elementos do cartaz ✅
* Numeração na tabela de catálogo de produtos ✅
* Mudança no botão marcar/desmarcar de check box para radio box ✅
* Fazer aparecer o nome do arquivo de excel usado na tebela de catálogo de produtos ✅
* Permiti a importação e exportação de templates no sistema ✅

#### 18/05/2018 [v0.4]
* Corrigir ajuste de tamanho de texto em folha A4 ✅
* Corrigir limite de memória para exportação de folhas A1 ❌
* Corrigir problema de cartaz duplicado ✅
* Criar opção de exportar em imagem ✅

#### 24/05/2018 [v0.5]
* Permitir exportação dos cartazes em imagem ✅
* Adicionar opção de configurar borda de elemento geométrico ✅
* Adicionar opção de tornar elemento geométrico inclinado ✅
* Tornar a cor do texto dos cartazes preto no documento PDF ✅

#### 28/09/2018 [v0.6]
* Adicionar opção de recuperação de conta ✅
* Adicionar tempo de licensa de validação ✅
* Permitir exportar banco de dados dos templates sem login ✅

#### 24/05/2019 [v0.7]
* Ajustes na licensa de validação ✅

	
Vprint &copy 2018 - Sistema de criação de gerenciamento de cartazes

