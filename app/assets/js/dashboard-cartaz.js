var height = 0;
var width = 0;
var id_template = $("#id_template").val();
var selecionados = false;

function ajax_inject(tipo = 'folha'){
	var tag  = $("#"+tipo);
	$.ajax({
		url: URL_RELATIVA+'cartazhtml/ajax_save_cartaz_html',  
		type: 'POST',
    	data: {id:id_template, cartaz_html:tag.html()},
    	dataType: "text",
		success: function(result){
			console.log(result);
    	}
    });
}

function ajax_set_sheet_size(tamanho_px, tamanho_cm = '', orientacao_cartaz = 'retrato'){
	console.log('edit');
	$.ajax({
		url: URL_RELATIVA+'dashboard/ajax_set_size',  
		type: 'POST',
    	data: {tamanho_cartaz_px:tamanho_px, tamanho_cartaz_cm:tamanho_cm, id:id_template, orientacao:orientacao_cartaz},
    	dataType: "text",
		success: function(result){
			console.log( result );
    }});

}

function numProps(obj) {
  var c = 0;
  for (var key in obj) {
    if (obj.hasOwnProperty(key)) ++c;
  }
  return c;
}

$(document).ready(function(){
	var scala = $("#cartaz-final").attr('data-scala');
	if (scala == 1) {
		$("#cartaz-final").css('border', '10px solid #AFAFAF');
		$("#cartaz-final").css('box-shadow', '7px 10px 7px 7px #888888');
	} else if (scala == 2) {
		$("#cartaz-final").css('border', '9px solid #AFAFAF');
		$("#cartaz-final").css('box-shadow', '6px 9px 6px 6px #888888');
	} else if (scala == 3) {
		$("#cartaz-final").css('border', '8px solid #AFAFAF');
		$("#cartaz-final").css('box-shadow', '5px 8px 5px #888888');
	} else if (scala == 4) {
		$("#cartaz-final").css('border', '2px solid #AFAFAF');
		$("#cartaz-final").css('box-shadow', '4px 7px 4px #888888');
	} else if (scala == 5) {
		$("#cartaz-final").css('border', '2px solid #AFAFAF');
		$("#cartaz-final").css('box-shadow', '3px 6px 3px #888888');
	} else if (scala == 6) {
		$("#cartaz-final").css('border', '2px solid #AFAFAF');
		$("#cartaz-final").css('box-shadow', '3px 6px 3px #888888');
	}
});

$(document).ready(function(){
	$.ajax({
		url: URL_RELATIVA+'cartazhtml/ajax_get_produtos',  
		type: 'POST',
    	dataType: "text",
		success: function(result){
			result = JSON.parse(result);
			if (result.modo == 'unico') {
				var id = 0;
				$.each( result.produtos, function( key, value ) {
	   				$.each( value, function( key1, value1) {
	   					if ($(".folha #header #elemento-"+id).length == 0) {
						  	$(".folha #header").append('<div class="elemento" id="elemento-'+id+'" data-elemento="'+id+'"><div class="fonte">'+value1+'</div></div>');
							$("#elemento-"+id).draggable();
							$("#elemento-"+id).resizable({
								 handles: "all"
							});
						} else {
	   						$(".folha #header #elemento-"+id).html(value1);
						}
	   					id++;
	   				});
	   			});

	   			elementos();
				textos();
				imagens();
				geometricos();
			} else {
				var em_pagina = $("#cartaz-final #folha div.folha-interna").length;

				if (em_pagina > 0) {
					var prod = numProps(result.produtos);
					var paginas = parseInt(prod/em_pagina);
					var indice_interno = 0;

					if (paginas >= 1) {
						for (var i = 0; i < paginas-1; i++) {
		   					$("#folha").clone().appendTo("#cartaz-final");
						};
						$.each($("#cartaz-final #folha #cartaz-0"), function(key, value){
							if (key == 0) {
								$(this).parent().attr('id','folha');
							} else {
								$(this).parent().attr('id','folha-'+(key-1));
							}
						})
					}

					var pag_cartaz = -1;
					var acumulador = 0;
					$.each( result.produtos, function( key, value ) {
						if (key <= 3) {
							id = "#folha #cartaz-"+indice_interno;
						} else {
							if (pag_cartaz == -1) {
								id = "#folha #cartaz-"+indice_interno;	
							} else {
								id = "#folha-"+pag_cartaz+" #cartaz-"+indice_interno;
							}
						}
						
						if (indice_interno <= 2) {
							indice_interno++;
						} else {
							indice_interno = 0;
						}

						// $(".texto, .imagem, .geometrico").draggable();
						// $(".texto, .imagem, .geometrico").resizable({
						// 	 handles: "all"
						// });
						$.each( value, function( key1, value1) {
		   					if ($(id+" #header #elemento-"+key1+" div.fonte").length == 0) {
							  	$(id+" #header").append('<div class="elemento" id="elemento-'+key1+'" data-elemento="'+key1+'"><div class="fonte">'+value1+'</div></div>');
								// $(id+" #header #elemento-"+key1).draggable();
								// $(id+" #header #elemento-"+key1).resizable({
								// 	 handles: "all"
								// });
							} else {
		   						$(id+" #header #elemento-"+key1+" div.fonte").html(value1);
		   			// 			$(id+" #header #elemento-"+key1).draggable();
								// $(id+" #header #elemento-"+key1).resizable({
								// 	 handles: "all"
								// });
							}

							if (key1 != 6 && key1 != 7 && key1 != 8 && key1 != 9) {
							    while( $(id+" #header #elemento-"+key1+" div.fonte").height() > $(id+" #header #elemento-"+key1).height() ) {
									$(id+" #header #elemento-"+key1+" div.fonte").css('font-size', (parseInt($(id+" #header #elemento-"+key1+" div.fonte").css('font-size')) - 1) + "px" );
								}
							}
		   				});
						
						acumulador++;
						if (acumulador == 4) {
							pag_cartaz++;
							acumulador = 0;
						};
						
					});

				} else {
					var indice = 0;

					$.each( result.produtos, function( key, value ) {
		   				if (indice > 0) {
		   					$("#cartaz-final").append('<div id="cartaz-'+key+'" class="folha" style="'+$("#folha").attr('style')+'"></div>');
		   					$("#folha #header").clone().appendTo("#cartaz-"+key);
		   					$("#folha #body").clone().appendTo("#cartaz-"+key);
		   					$("#folha #footer").clone().appendTo("#cartaz-"+key);
		   				}
	   					if (indice == 0) {
	   						id = "#folha";
	   					} else {
	   						id = "#cartaz-"+key;
	   					}

	   					$(".texto, .imagem, .geometrico").draggable();
						$(".texto, .imagem, .geometrico").resizable({
							 handles: "all"
						});
	   					
		   				$.each( value, function( key1, value1) {

		   					if ($(id+" #header #elemento-"+key1+" div.fonte").length == 0) {
							  	$(id+" #header").append('<div class="elemento" id="elemento-'+key1+'" data-elemento="'+key1+'"><div class="fonte">'+value1+'</div></div>');
								$(id+" #header #elemento-"+key1).draggable();
								$(id+" #header #elemento-"+key1).resizable({
									 handles: "all"
								});
							} else {
		   						$(id+" #header #elemento-"+key1+" div.fonte").html(value1);
		   						$(id+" #header #elemento-"+key1).draggable();
								$(id+" #header #elemento-"+key1).resizable({
									 handles: "all"
								});
							}

							if (key1 != 6 && key1 != 7 && key1 != 8 && key1 != 9) {
							    while( $(id+" #header #elemento-"+key1+" div.fonte").height() > $(id+" #header #elemento-"+key1).height() ) {
									$(id+" #header #elemento-"+key1+" div.fonte").css('font-size', (parseInt($(id+" #header #elemento-"+key1+" div.fonte").css('font-size')) - 1) + "px" );
								}
							};
		   				});
		   				indice++;
		   			});
				}
				
				elementos();
				textos();
				imagens();
				geometricos();
			}
    }});
});

