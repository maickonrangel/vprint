$("#header").dblclick(function(e){
	e.stopPropagation();
	$("#config-header").fadeOut(500);
	$("#config-header").fadeIn(500);
	$("#config-header").css({"display":"block", "box-shadow":"0 1px 3px 1px rgba(0, 0, 0, 0.5)"});
	$(".close").css({"display":"block"});
	$(".config-text").html('Cabeçalho');
	$("#ideal-size-header").html("Tamanho cabeçalho: "+$("#header").width()+'x'+$("#header").height());
	$("#height-header").val($("#header").height());
});

$("#body").dblclick(function(e){
	e.stopPropagation();
	$("#config-body").fadeOut(500);
	$("#config-body").fadeIn(500);
	$("#config-body").css({"display":"block", "box-shadow":"0 1px 3px 1px rgba(0, 0, 0, 0.5)"});
	$(".close").css({"display":"block"});
	$("#ideal-size-body").html("Tamanho corpo: "+$("#body").width()+'x'+$("#body").height());
	$("#height-body").val($("#body").height());
});

$("#footer").dblclick(function(e){
	e.stopPropagation();
	$("#config-footer").fadeOut(500);
	$("#config-footer").fadeIn(500);
	$("#config-footer").css({"display":"block", "box-shadow":"0 1px 3px 1px rgba(0, 0, 0, 0.5)"});
	$(".close").css({"display":"block"});
	$("#ideal-size-footer").html("Tamanho rodapé: "+$("#footer").width()+'x'+$("#footer").height());
	$("#height-footer").val($("#footer").height());
});

function geometricos() {
	$(".folha #header").on('dblclick', 'div.geometrico', function(e){
		e.stopPropagation();
		$("#config-geometric-control").fadeOut(500);
		$("#config-geometric-control").fadeIn(500);
		$(".config-text").html('Elemento: Geométrico #'+$(this).attr('data-geometrico'));
		$("#config-geometrico-control").css({"display":"block", "box-shadow":"0 1px 3px 1px rgba(0, 0, 0, 0.5)"});
		$(".close").css({"display":"block"});
		// ----	
		$("#id-geometrico").val($(this).attr('data-geometrico'));
		$("#indice-cartaz-geometrico").val("#"+$(this).parent().parent().parent().attr('id')+" #"+$(this).parent().parent().attr('id')+" #header");
		$("#width-geometrico").val($(this).width());
		$("#height-geometrico").val($(this).height());
		$("#border-geometrico").val($(this).css('border-radius').replace('px',''));
		 // check sobreposição
		if ($(this).css('z-index') == '999') {
			$("#geometrico-sobre").prop("checked", true);
		} else if ($(this).css('z-index') == '99') {
			$("#geometrico-abaixo").prop("checked", true);
		} else {
			$("#geometrico-sobre").prop("checked", true);
		}
	});
}

function elementos(){
	$(".folha #header").on('dblclick', 'div.elemento', function(e){
		e.stopPropagation();
		$("#config-control").fadeOut(500);
		$("#config-control").fadeIn(500);
		$(".config-text").html('Elemento: Texto #'+$(this).attr('data-elemento'));
		$("#config-control").css({"display":"block", "box-shadow":"0 1px 3px 1px rgba(0, 0, 0, 0.5)"});
		$(".close").css({"display":"block"});
		// -----
		$("#id-elemento").val($(this).attr('data-elemento'));
		$("#indice-cartaz").val("#"+$(this).parent().parent().parent().attr('id')+" #"+$(this).parent().parent().attr('id')+" #header");
		$("#name-product").val($(this).text());
		$("#font-product-size").val($(this).css('font-size').replace('px',''));
		var font_family = $(this).css("font-family");
		$("#font-family-product-style option").each(function(){
			if ($(this).val() != '') {
				font_family = font_family.replace('"','').replace('"','');
		        if ($(this).val() == font_family) {
		            $(this).attr("selected","selected");    
		        }
			};
	    });
		$("#div-width").val($(this).width());
		$("#div-height").val($(this).height());
		// check font
		if ($(this).css('font-weight') == 700) {
			$("#font-product-bold").prop("checked", true);
		} else {
			$("#font-product-bold").prop("checked", false);
		}
		// check text-transform
		if ($(this).css('text-transform') == 'uppercase') {
			$("#font-product-uppercase").prop("checked", true);
		} else {
			$("#font-product-uppercase").prop("checked", false);
		}
		// check display remove
		if ($(this).css('display') == 'none') {
			$("#product-remove").prop("checked", true);
		} else {
			$("#product-remove").prop("checked", false);
		}
		// check display recover
		if ($(this).css('display') == 'block') {
			$("#product-recover").prop("checked", true);
		} else {
			$("#product-recover").prop("checked", false);
		}
		// check text-align
		if ($(this).css('text-align') == 'center') {
			$("#font-product-center").prop("checked", true);
		} else if ($(this).css('text-align') == 'left') {
			$("#font-product-left").prop("checked", true);
		} else if ($(this).css('text-align') == 'right') {
			$("#font-product-right").prop("checked", true);
		}
	});
}

function textos() {
	$(".folha #header").on('dblclick', 'div.texto', function(e){
		e.stopPropagation();
		$("#config-text-control").fadeOut(500);
		$("#config-text-control").fadeIn(500);
		$(".config-text").html('Elemento: Texto #'+$(this).attr('data-texto'));
		$("#config-text-control").css({"display":"block", "box-shadow":"0 1px 3px 1px rgba(0, 0, 0, 0.5)"});
		$(".close").css({"display":"block"});
		// ----
		$("#indice-cartaz-texto").val("#"+$(this).parent().parent().parent().attr('id')+" #"+$(this).parent().parent().attr('id')+" #header");
		$("#id-texto").val($(this).attr('data-texto'));
		$("#name-texto").val($(this).text());
		$("#div-width-texto").val($(this).width());
		$("#div-height-texto").val($(this).height());
		$("#font-texto-size").val($(this).css('font-size').replace('px',''));
		var font_family = $(this).css("font-family");
		$("#font-family-texto-style option").each(function(){
			if ($(this).val() != '') {
				font_family = font_family.replace('"','').replace('"','');
		        if ($(this).val() == font_family) {
		            $(this).attr("selected","selected");    
		        }
			};
	    });
		// check font
		if ($(this).css('font-weight') == 700) {
			$("#font-texto-bold").prop("checked", true);
		} else {
			$("#font-texto-bold").prop("checked", false);
		}
		// check text-transform
		if ($(this).css('text-transform') == 'uppercase') {
			$("#font-texto-uppercase").prop("checked", true);
		} else {
			$("#font-texto-uppercase").prop("checked", false);
		}
		// check display remove
		if ($(this).css('display') == 'none') {
			$("#texto-remove").prop("checked", true);
		} else {
			$("#texto-remove").prop("checked", false);
		}
		// check display recover
		if ($(this).css('display') == 'block') {
			$("#texto-recover").prop("checked", true);
		} else {
			$("#texto-recover").prop("checked", false);
		}
		// check text-align
		if ($(this).css('text-align') == 'center') {
			$("#font-texto-center").prop("checked", true);
		} else if ($(this).css('text-align') == 'left') {
			$("#font-texto-left").prop("checked", true);
		} else if ($(this).css('text-align') == 'right') {
			$("#font-texto-right").prop("checked", true);
		}
	});
}

function imagens(){
	$(".folha #header").on('dblclick', 'div.imagem', function(e){
		e.stopPropagation();
		$("#config-imagem-control").fadeOut(500);
		$("#config-imagem-control").fadeIn(500);
		$(".config-text").html('Elemento: Imagem #'+$(this).attr('data-imagem'));
		$("#config-imagem-control").css({"display":"block", "box-shadow":"0 1px 3px 1px rgba(0, 0, 0, 0.5)"});
		$(".close").css({"display":"block"});
		// ----
		$("#indice-cartaz-imagem").val("#"+$(this).parent().parent().parent().attr('id')+" #"+$(this).parent().parent().attr('id')+" #header");
		$("#id-imagem").val($(this).attr('data-imagem'));
		$("#width-imagem").val($(this).width());
		$("#height-imagem").val($(this).height());
		var bg = $(this).css("background-image");
		$("#imagem-add option").each(function(){
			if ($(this).val() != '') {
				bg = bg.replace('url("','').replace('")','');
		        if ($(this).val() == bg) {
		            $(this).attr("selected","selected");    
		        }
			};
	    });
	    // check sobreposição
		if ($(this).css('z-index') == '999') {
			$("#imagem-sobre").prop("checked", true);
		} else if ($(this).css('z-index') == '99') {
			$("#imagem-abaixo").prop("checked", true);
		} else {
			$("#imagem-sobre").prop("checked", true);
		}
	});
	
}