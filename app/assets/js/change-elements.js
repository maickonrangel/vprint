// config control elemento de texto
$("#name-product").keyup(function(){
	var id_elemento = $("#id-elemento").val();
	var id = $("#indice-cartaz").val()+" #elemento-"+id_elemento;
	
	while( $(id+" div.fonte").height() > $(id).height() ) {
		$(id+" div.fonte").css('font-size', (parseInt($(id+" div.fonte").css('font-size')) - 5) + "px" );
	}
	var name = $("#name-product").val();
	$(id+" div.fonte").text(name);
});
$("#color-palet-product").change(function(){
	var id = $("#indice-cartaz").val()+" #elemento-"+$("#id-elemento").val();
	$(id).css("color",$("#color-palet-product").val());
});
$("#font-product-size").keyup(function(){
	var id = $("#indice-cartaz").val()+" #elemento-"+$("#id-elemento").val();
	$(id+" div.fonte").css("font-size",$("#font-product-size").val());
	$(id).css("font-size",$("#font-product-size").val());
});
$("#font-family-product-style").change(function(){
	var id = $("#indice-cartaz").val()+" #elemento-"+$("#id-elemento").val();
	$(id).css("font-family",$("#font-family-product-style").val());
});
$("#div-width").keyup(function(){
	var id = $("#indice-cartaz").val()+" #elemento-"+$("#id-elemento").val();
	$(id).css('border','1px dashed red');
	$(id).css("width",$("#div-width").val());
});
$("#div-width").keydown(function(){
	var id = $("#indice-cartaz").val()+" #elemento-"+$("#id-elemento").val();
	$(id).css('border','none');
	$(id).hover(function(){
		$(this).css('border','1px dashed red');
	}, function(){
		$(this).css('border','none');
	});
});
$("#div-height").keyup(function(){
	var id = $("#indice-cartaz").val()+" #elemento-"+$("#id-elemento").val();
	$(id).css('border','1px dashed red');
	$(id).css("height",$("#div-height").val());
});
$("#div-height").keydown(function(){
	var id = $("#indice-cartaz").val()+" #elemento-"+$("#id-elemento").val();
	$(id).css('border','none');
	$(id).hover(function(){
		$(this).css('border','1px dashed red');
	}, function(){
		$(this).css('border','none');
	});
});
$("#font-product-bold").change(function(){
	var id = $("#indice-cartaz").val()+" #elemento-"+$("#id-elemento").val();
	var n = $("#font-product-bold:checked").length;
	if (n == 1) {
		$(id).css("font-weight","bold");
	} else {
		$(id).css("font-weight","normal");
	};
});
$("#font-product-uppercase").change(function(){
	var id = $("#indice-cartaz").val()+" #elemento-"+$("#id-elemento").val();
	var n = $("#font-product-uppercase:checked").length;
	if (n == 1) {
		$(id).css("text-transform","uppercase");
	} else {
		$(id).css("text-transform","lowercase");
	};
});
$("#font-product-center").change(function(){
	var id = $("#indice-cartaz").val()+" #elemento-"+$("#id-elemento").val();
	var n = $("#font-product-center:checked").length;
	if (n == 1) {
		$(id).css("text-align","center");
	} else {
		$(id).css("text-align","normal");
	};
});
$("#font-product-left").change(function(){
	var id = $("#indice-cartaz").val()+" #elemento-"+$("#id-elemento").val();
	var n = $("#font-product-left:checked").length;
	if (n == 1) {
		$(id).css("text-align","left");
	} else {
		$(id).css("text-align","center");
	};
});

$("#font-product-right").change(function(){
	var id = $("#indice-cartaz").val()+" #elemento-"+$("#id-elemento").val();
	var n = $("#font-product-right:checked").length;
	if (n == 1) {
		$(id).css("text-align","right");
	} else {
		$(id).css("text-align","center");
	};
});
$("#product-remove").change(function(){
	var id = $("#indice-cartaz").val()+" #elemento-"+$("#id-elemento").val();
	var n = $("#product-remove:checked").length;
	if (n == 1) {
		$(id).css("display","none");
	} else {
		$(id).css("display","block");
	};
});
$("#product-recover").change(function(){
	var id = $("#indice-cartaz").val()+" .elemento";
	var n = $("#product-recover:checked").length;
	if (n == 1) {
		$(id).css("display","block");
	}
});

//***********************************************************//
//config texto - elemento de texto adicionado por componente //
//***********************************************************//
$("#name-texto").keyup(function(){
	var name = $("#name-texto").val();
	var id = $("#indice-cartaz-texto").val()+" #texto-"+$("#id-texto").val();
	while( $(id+" div.fonte").height() > $(id).height() ) {
		$(id+" div.fonte").css('font-size', (parseInt($(id+" div.fonte").css('font-size')) - 1) + "px" );
	}
	$(id+" div.fonte").text(name);
});
$("#color-palet-texto").change(function(){
	var id = $("#indice-cartaz-texto").val()+" #texto-"+$("#id-texto").val();
	$(id).css("color",$("#color-palet-texto").val());
});
$("#font-texto-size").keyup(function(){
	var id = $("#indice-cartaz-texto").val()+" #texto-"+$("#id-texto").val();
	$(id).css("font-size",$("#font-texto-size").val());
	$(id+" div.fonte").css("font-size",$("#font-texto-size").val());
});
$("#font-family-texto-style").change(function(){
	var id = $("#indice-cartaz-texto").val()+" #texto-"+$("#id-texto").val();
	$(id).css("font-family",$("#font-family-texto-style").val());
});
$("#div-width-texto").keyup(function(){
	var id = $("#indice-cartaz").val()+" #texto-"+$("#id-texto").val();
	$(id).css('border','1px dashed red');
	$(id).css("width",$("#div-width-texto").val());
});
$("#div-width-texto").keydown(function(){
	var id = $("#indice-cartaz").val()+" #texto-"+$("#id-texto").val();
	$(id).css('border','none');
	$(id).hover(function(){
		$(this).css('border','1px dashed red');
	}, function(){
		$(this).css('border','none');
	});
});
$("#div-height-texto").keyup(function(){
	var id = $("#indice-cartaz").val()+" #texto-"+$("#id-texto").val();
	$(id).css('border','1px dashed red');
	$(id).css("height",$("#div-height-texto").val());
});
$("#div-height-texto").keydown(function(){
	var id = $("#indice-cartaz").val()+" #texto-"+$("#id-texto").val();
	$(id).css('border','none');
	$(id).hover(function(){
		$(this).css('border','1px dashed red');
	}, function(){
		$(this).css('border','none');
	});
});
$("#font-texto-bold").change(function(){
	var id = $("#indice-cartaz-texto").val()+" #texto-"+$("#id-texto").val();
	var n = $("#font-texto-bold:checked").length;
	if (n == 1) {
		$(id).css("font-weight","bold");
	} else {
		$(id).css("font-weight","normal");
	};
});
$("#font-texto-uppercase").change(function(){
	var id = $("#indice-cartaz-texto").val()+" #texto-"+$("#id-texto").val();
	var n = $("#font-texto-uppercase:checked").length;
	if (n == 1) {
		$(id).css("text-transform","uppercase");
	} else {
		$(id).css("text-transform","lowercase");
	};
});
$("#font-texto-center").change(function(){
	var id = $("#indice-cartaz-texto").val()+" #texto-"+$("#id-texto").val();
	var n = $("#font-texto-center:checked").length;
	if (n == 1) {
		$(id).css("text-align","center");
	} else {
		$(id).css("text-align","normal");
	};
});
$("#font-texto-left").change(function(){
	var id = $("#indice-cartaz-texto").val()+" #texto-"+$("#id-texto").val();
	var n = $("#font-texto-left:checked").length;
	if (n == 1) {
		$(id).css("text-align","left");
	} else {
		$(id).css("text-align","center");
	};
});
$("#font-texto-right").change(function(){
	var id = $("#indice-cartaz-texto").val()+" #texto-"+$("#id-texto").val();
	var n = $("#font-texto-right:checked").length;
	if (n == 1) {
		$(id).css("text-align","right");
	} else {
		$(id).css("text-align","center");
	};
});
$("#texto-remove").change(function(){
	var id = $("#indice-cartaz-texto").val()+" #texto-"+$("#id-texto").val();
	var n = $("#texto-remove:checked").length;
	if (n == 1) {
		$(id).remove();
	}
});
$("#texto-remove-all").change(function(){
	var id = $("#indice-cartaz-texto").val()+" .texto";
	var n = $("#texto-remove-all:checked").length;
	if (n == 1) {
		$(id).remove();
	}
});
$("#texto-hide").change(function(){
	var id = $("#indice-cartaz-texto").val()+" #texto-"+$("#id-texto").val();
	var n = $("#texto-hide:checked").length;
	if (n == 1) {
		$(id).css('display','none');
	}
});
$("#texto-recover").change(function(){
	var id = $("#indice-cartaz-texto").val()+" .texto";
	var n = $("#texto-recover:checked").length;
	if (n == 1) {
		$(id).css('display','block');
	}
});
//componente imagem
$("#color-palet-imagem").change(function(){
	var id = $("#indice-cartaz-imagem").val()+" #imagem-"+$("#id-imagem").val();
	$(id).css("background-color",$("#color-palet-imagem").val());
});
$("#width-imagem").keyup(function(){
	var width = $("#width-imagem").val();
	var id = $("#indice-cartaz-imagem").val()+" #imagem-"+$("#id-imagem").val();
	$(id).css('width',width+'px');
});
$("#height-imagem").keyup(function(){
	var width = $("#height-imagem").val();
	var id = $("#indice-cartaz-imagem").val()+" #imagem-"+$("#id-imagem").val();
	$(id).css('height',width+'px');
});
$("#imagem-add").change(function(){
	var imagem = $("#imagem-add").val();
	var id = $("#indice-cartaz-imagem").val()+" #imagem-"+$("#id-imagem").val();
	$(id).css('background-image', "url("+imagem+")");
});
$("#imagem-sobre").change(function(){
	var id = $("#indice-cartaz-imagem").val()+" #imagem-"+$("#id-imagem").val();
	var n = $("#imagem-sobre:checked").length;
	if (n == 1) {
		$(id).css('z-index','999');
	}
});
$("#imagem-abaixo").change(function(){
	var id = $("#indice-cartaz-imagem").val()+" #imagem-"+$("#id-imagem").val();
	var n = $("#imagem-abaixo:checked").length;
	if (n == 1) {
		$(id).css('z-index','99');
	}
});
$("#imagem-remove").change(function(){
	var id = $("#indice-cartaz-imagem").val()+" #imagem-"+$("#id-imagem").val();
	var n = $("#imagem-remove:checked").length;
	if (n == 1) {
		$(id).remove();
	}
});

// componente geometrico
$("#color-palet-geometrico").change(function(){
	var id = $("#indice-cartaz-geometrico").val()+" #geometrico-"+$("#id-geometrico").val();
	$(id).css("background-color",$("#color-palet-geometrico").val());
});
$("#width-geometrico").keyup(function(){
	var width = $("#width-geometrico").val();
	var id = $("#indice-cartaz-geometrico").val()+" #geometrico-"+$("#id-geometrico").val();
	$(id).css('width',width+'px');
});
$("#height-geometrico").keyup(function(){
	var height = $("#height-geometrico").val();
	var id = $("#indice-cartaz-geometrico").val()+" #geometrico-"+$("#id-geometrico").val();
	$(id).css('height',height+'px');
});
$("#border-geometrico").keyup(function(){
	var border = $("#border-geometrico").val();
	var id = $("#indice-cartaz-geometrico").val()+" #geometrico-"+$("#id-geometrico").val();
	$(id).css('border-radius',border+'px');
});
$("#border-espessura-geometrico").keyup(function(){
	var espessura = $("#border-espessura-geometrico").val();
	var id = $("#indice-cartaz-geometrico").val()+" #geometrico-"+$("#id-geometrico").val();
	$(id).css('border',espessura+'px solid rgb(0, 0, 0)');
});
$("#border-inclinacao-geometrico").keyup(function(){
	var inclinacao = $("#border-inclinacao-geometrico").val();
	var id = $("#indice-cartaz-geometrico").val()+" #geometrico-"+$("#id-geometrico").val();
	$(id).css('-webkit-transform','skew('+inclinacao+'deg)');
});
$("#color-palet-border-geometrico").change(function(){
	var espessura = $("#border-espessura-geometrico").val();
	var id = $("#indice-cartaz-geometrico").val()+" #geometrico-"+$("#id-geometrico").val();
	$(id).css("border",espessura+'px solid '+$("#color-palet-border-geometrico").val());
});
$("#geometrico-esfera-l").change(function(){
	var id = $("#indice-cartaz-geometrico").val()+" #geometrico-"+$("#id-geometrico").val();
	var n = $("#geometrico-esfera-l:checked").length;
	if (n == 1) {
		$(id).css('height', $(id).width());
		$(id).css('border-radius',4500);
		$("#height-geometrico").val($(id).width());
		$("#border-geometrico").val($(id).css('border-radius').replace('px',''));
	}
});
$("#geometrico-esfera-a").change(function(){
	var id = $("#indice-cartaz-geometrico").val()+" #geometrico-"+$("#id-geometrico").val();
	var n = $("#geometrico-esfera-a:checked").length;
	if (n == 1) {
		$(id).css('width', $(id).height());
		$(id).css('border-radius',4500);
		$("#width-geometrico").val($(id).height());
		$("#border-geometrico").val($(id).css('border-radius').replace('px',''));
	}
});
$("#geometrico-remove").change(function(){
	var id = $("#indice-cartaz-geometrico").val()+" #geometrico-"+$("#id-geometrico").val();
	var n = $("#geometrico-remove:checked").length;
	if (n == 1) {
		$(id).remove();
	}
});
$("#geometrico-sobre").change(function(){
	var id = $("#indice-cartaz-geometrico").val()+" #geometrico-"+$("#id-geometrico").val();
	var n = $("#geometrico-sobre:checked").length;
	if (n == 1) {
		$(id).css('z-index',999);
	}
});
$("#geometrico-abaixo").change(function(){
	var id = $("#indice-cartaz-geometrico").val()+" #geometrico-"+$("#id-geometrico").val();
	var n = $("#geometrico-abaixo:checked").length;
	if (n == 1) {
		$(id).css('z-index',99);
	}
});

// config header
$("#color-palet-header").change(function(){
	$("#header").css("background-image", "none");
	$("#header").css("background-color",$("#color-palet-header").val());
});
$("#height-header").change(function(){
	$("#header").css("height", $("#height-header").val()+"px");
});
$("#image-header").change(function(){
	var image = "'"+$("#image-header").val()+"'";
	$("#header").css("background-image", "url("+image+")");
	$("#header").css("background-size", "100% 100%");
	$("#header").css("background-color","none");
});

// config body
$("#color-palet-body").change(function(){
	$("#body").css("background-image", "none");
	$("#body").css("background-color",$("#color-palet-body").val());
});
$("#height-body").change(function(){
	$("#body").css("height", $("#height-body").val()+"px");
});
$("#image-body").change(function(){
	var image = $("#image-body").val();
	$("#body").css("background-image", "url('"+image+"')");
	$("#body").css("background-size", "100% 100%");
	$("#body").css("background-color","none");
});

// config footer
$("#color-palet-footer").change(function(){
	$("#footer").css("background-image", "none");
	$("#footer").css("background-color",$("#color-palet-footer").val());
});
$("#height-footer").keyup(function(){
	$("#footer").css("height", $("#height-footer").val()+"px");
});
$("#image-footer").change(function(){
	var image = $("#image-footer").val();
	$("#footer").css("background-image", "url('"+image+"')");
	$("#footer").css("background-size", "100% 100%");
	$("#footer").css("background-color","none");
});

$("#file-template").change(function(){
	$.ajax({
		url: URL_RELATIVA+'dashboard/ajax_select_html',  
		type: 'POST',
    	data: {id: $("#file-template").val()},
    	dataType: "text",
		success: function(result){
			result = JSON.parse(result);
			var tamanho = $("#file-template option:selected").data('tamanho').split('x');
			$("#cartaz-final").empty();
			$("#cartaz-final").append('<input type="hidden" id="id_template" value="'+id_template+'">');
			for (var i = 0; i < $("#cartazes-qtd").val(); i++) {
				if ($("#cartazes-qtd").val() == 1) {
					$("#cartaz-final").append(result);
					$("#cartaz-final #id_template").remove();
					$("#cartaz-final").append('<input type="hidden" id="id_template" value="'+id_template+'">');
				} else {
					$("#cartaz-final").append('<div id="cartaz-'+i+'" class="folha-interna" data-cartaz="cartaz-'+i+'">'+result+'<div>');
					$("#cartaz-"+i+" #id_template").remove();
				}
				$("#cartaz-"+i).width(tamanho[0]+'px');
				$("#cartaz-"+i).height(tamanho[1]+'px');
				$("#cartaz-"+i).css('margin', '0px');
				$("#cartaz-"+i).css('margin-left', '1.6px');
				$("#cartaz-final").css('box-shadow','rgba(0, 0, 0, 0.5) 0px 1px 3px 1px');
			};
			ajax_inject('cartaz-final');
		}
    });
});

$("#clear").change(function(){
	var n = $("#clear:checked").length;
	if (n == 1) {
		$("#cartaz-final").empty();
	} else {
		if($("#folha").length == 0) {
			$("#cartaz-final").append('<div id="folha-0" class="folha-interna"></div>');
			$("#folha").append('<input type="hidden" id="id_template" value="'+id_template+'">');
			$("#folha").append('<div id="header"></div>');
			$("#folha").append('<div id="body"></div>');
			$("#folha").append('<div id="footer"></div>');
			$("#folha").css('box-shadow','rgba(0, 0, 0, 0.5) 0px 1px 3px 1px');
		}
	} 
});

$("#file_type").change(function(){
	var file_type = $("#file_type").val();
	$.ajax({
		url: URL_RELATIVA+'dashboard/ajax_get_files',  
		type: 'POST',
    	data: {type: file_type},
    	dataType: "text",
		success: function(result){
			var result = JSON.parse( result );
	       	var select_nome = $("#file_list");
	       	select_nome.empty();
			select_nome.append('<option></select>');
	       	$.each( result, function( key, value ) {
				select_nome.append('<option value="'+value+'">'+value+'</select>');
			});
    	}
    });
});

$("#marcar-tudo").change(function(){
	var n = $("#marcar-tudo:checked").length;
	if (n == 1) {
		$(".checkbox-produto").prop('checked','false');
		$.each($(".checkbox-produto"), function(key, value){
			var url = $("#checkbox-"+key).attr("data-url");
			var id = $("#checkbox-"+key).attr("data-id");
			$.ajax({
				url: URL_RELATIVA+'dashboard/ajax_add_products',  
				type: 'POST',
		    	data: {data_url:url, data_id:id},
		    	dataType: "text",
				success: function(result){
					console.log( result );
		    }});
		});
	} else {
		$(".checkbox-produto").removeAttr('checked');
		$.each($(".checkbox-produto"), function(key, value){
			var url = $("#checkbox-"+key).attr("data-url");
			var id = $("#checkbox-"+key).attr("data-id");
			$.ajax({
				url: URL_RELATIVA+'dashboard/ajax_remove_products',  
				type: 'POST',
		    	data: {data_url:url, data_id:id},
		    	dataType: "text",
				success: function(result){
					console.log( result );
		    }});
		});
	};
});

$("#desmarcar-tudo").change(function(){
	var n = $(this).length;
	if (n == 1) {
		$(".checkbox-produto").removeAttr('checked');
		$.each($(".checkbox-produto"), function(key, value){
			var url = $("#checkbox-"+key).attr("data-url");
			var id = $("#checkbox-"+key).attr("data-id");
			$.ajax({
				url: URL_RELATIVA+'dashboard/ajax_remove_products',  
				type: 'POST',
		    	data: {data_url:url, data_id:id},
		    	dataType: "text",
				success: function(result){
					console.log( result );
		    }});
		});
	}
});

$("#duplicar-produtos").change(function(){
	var n = $("#duplicar-produtos:checked").length;
	if (n == 1) {
		$(".qtd-products").css('display','block');
	} else {
		$(".qtd-products").css('display','none');
		$(".checkbox-produto").removeAttr('checked');
		$.ajax({
			url: URL_RELATIVA+'dashboard/ajax_clear_products',  
			type: 'POST',
	    	dataType: "text",
			success: function(result){
				console.log( result );
	    }});
	}
});

$(".checkbox-produto").change(function(){
	var url = $(this).attr("data-url");
	var id = $(this).attr("data-id");
	var n = $('#checkbox-'+id+':checkbox:checked').length;
	var prod = $("#duplicar-produtos:checked").length;
	var qtd_products = $("#qtd-products-option-"+id).val();
	if (n == 1 && prod == 1) {
		$.ajax({
			url: URL_RELATIVA+'dashboard/ajax_add_products',  
			type: 'POST',
	    	data: {data_url:url, data_id:id, qtd:qtd_products},
	    	dataType: "text",
			success: function(result){
				console.log( result );
	    }});
	} else if (n == 1){
		$.ajax({
			url: URL_RELATIVA+'dashboard/ajax_add_products',  
			type: 'POST',
	    	data: {data_url:url, data_id:id},
	    	dataType: "text",
			success: function(result){
				console.log( result );
	    }});
	} else {
		$.ajax({
			url: URL_RELATIVA+'dashboard/ajax_remove_products',  
			type: 'POST',
	    	data: {data_url:url, data_id:id},
	    	dataType: "text",
			success: function(result){
				console.log( result );
	    }});
	};
});

$("#mesmo-cartaz").change(function(){
	var n = $("#mesmo-cartaz:checked").length;
	if (n == 1) {
		$.ajax({
			url: URL_RELATIVA+'cartazhtml/ajax_set_modo_cartaz',  
			type: 'POST',
	    	data: {modo:'unico'},
	    	dataType: "text",
			success: function(result){
				console.log( result );
	    }});
	} else {
		$.ajax({
			url: URL_RELATIVA+'cartazhtml/ajax_set_modo_cartaz',  
			type: 'POST',
	    	data: {modo:'muitos'},
	    	dataType: "text",
			success: function(result){
				console.log( result );
	    }});
	};
});

$("#tamanho_cartaz_px_paisagem").change(function(){
	$("#tamanho_cartaz_cm").val($("#tamanho_cartaz_px_paisagem :selected").attr("data-cm"));
});

$(".cartaz_nome").change(function(){
	$.ajax({
		url: URL_RELATIVA+'cartazhtml/ajax_save_cartaz_html',  
		type: 'POST',
    	data: {id:$(this).attr('data-id'), nome_cartaz:$(this).val()},
    	dataType: "text",
		success: function(result){
			console.log( result );
    }});
});