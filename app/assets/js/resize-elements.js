$(".elemento, .imagem, .geometrico, .texto").mouseover(function(){
    $(this).resizable();
});

$(".cartaz-add .elemento").resizable('disable');
$(".cartaz-add .texto").resizable('disable');
$(".cartaz-add .geometrico").resizable('disable');
$(".cartaz-add .imagem").resizable('disable');

$("#header").resize(function() {
	$("#ideal-size-header").html("Tamanho cabeçalho: "+$("#header").width()+'x'+$("#header").height());
});

$("#body").resize(function() {
    $("#ideal-size-body").html("Tamanho corpo: "+$("#body").width()+'x'+$("#body").height());
    var ajust_footer = (($("#folha").height()) - ($("#header").height() + $("#body").height())).toFixed();
    $("#footer").css('height', parseInt(ajust_footer));
});

$("#footer").resize(function() {
    $("#ideal-size-footer").html("Tamanho rodapé: "+$("#footer").width()+'x'+$("#footer").height());
});
    
$("#header").resizable();
$("#header").resizable({
    handles: "n, s, ne",
    maxHeight: height,
    maxWidth: width,
    minWidth: width
});

$("#body").resizable();
$("#body").resizable({
    handles: "n, s, ne",
    maxHeight: height,
    maxWidth: width,
    minWidth: width
});

$("#footer").resizable();
$("#footer").resizable({
	handles: "n, s, ne",
  	maxHeight: height,
  	maxWidth: width,
  	minWidth: width
});
