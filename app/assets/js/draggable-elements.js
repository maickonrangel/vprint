$("#folha").on('drag', 'div.cartaz-add', function(){});

$("#folha").on('drag', 'div.texto', function(){});

$("#folha").on('drag', 'div.elemento', function(){});

$("#folha").on('drag', 'div.geometrico', function(){});

$("#folha").on('drag', 'div.imagem', function(){});

$(".cartaz-add").draggable('disable');
$(".cartaz-add .elemento").draggable('disable');
$(".cartaz-add .texto").draggable('disable');
$(".cartaz-add .geometrico").draggable('disable');
$(".cartaz-add .imagem").draggable('disable');
$(".elemento").draggable();
$(".geometrico").draggable();
$(".imagem").draggable();
$(".texto").draggable();

$(function(){
	$("#config-header, #config-body, #config-footer, #config-control, #config-text-control, #config-elemento, #config-geometrico, #config-imagem").draggable();
});

$(function(){
	$("#config-text").draggable();
});

$(function(){
	$(".footer-text").draggable();
});
