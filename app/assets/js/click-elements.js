var cartaz_nomes = [];
cartaz_nomes['3180x2246'] 	='A1';
cartaz_nomes['2246x3180'] 	='A1';
cartaz_nomes['1590x2246'] 	='A2';
cartaz_nomes['2246x1590'] 	='A2';
cartaz_nomes['1590x1123'] 	='A3';
cartaz_nomes['1123x1590'] 	='A3';
cartaz_nomes['795x1123' ]	='A4';
cartaz_nomes['1123x795' ]	='A4';
cartaz_nomes['559x794']		='A5';
cartaz_nomes['794x559']		='A5';
cartaz_nomes['559x397']		='A6';
cartaz_nomes['397x559']		='A6';

$("#config-cartaz").click(function(){
	$("#config-cartaz-control").css({"display":"block", "box-shadow":"0 1px 3px 1px rgba(0, 0, 0, 0.5)"});
	$(".close").css({"display":"block"});
	$(".config-text").html('Editar cartaz');
});

$(".botao").click(function (){
	ajax_inject();
});

$(".element-links-add").click(function(){
	var element = $(this).attr("data-element");
	var text = $(this).attr("data-text");

	$("#"+element).css('display','block');
	var count = $('.'+element).length;

	if (element == 'texto') {
		$("#folha #header").append('<div class="'+element+'" data-'+element+'="'+count+'" id="'+element+'-'+count+'"><div class="fonte">'+text+'</div></div>');
		$("."+element).draggable();
		$("."+element).resizable({
			 handles: "all"
		});
	} else if (element == 'imagem') {
		var style = 'data-'+element+'="'+count+'" id="'+element+'-'+count+'"';
		$("#folha #header").append('<div class="'+element+'" '+style+'></div>');
		$("."+element).resizable({
			 handles: "all"
		});
		$(".imagem").draggable();
	} else if (element == 'geometrico') {
		$("#folha #header").append('<div style="background:#000; height:5px;" class="'+element+'" data-'+element+'="'+count+'" id="'+element+'-'+count+'"></div>');
		$("."+element).draggable();
		$("."+element).resizable({
			 handles: "all"
		});
	}
});

$(".size-links").click(function(e){
	var size_px = $(this).attr("data-size-px");
	var size_cm = $(this).attr("data-size-cm");
	var orientacao = $(this).attr("data-orientacao");

	$("#status-cartaz-tipo-folha").html(cartaz_nomes[size_px]);
	
	size_px = size_px.split("x");
	size_cm = size_cm.split("x");
	$("#folha").css({"width":size_px[0], "height":size_px[1], "box-shadow":"0 1px 3px 1px rgba(0, 0, 0, 0.5)"});
	$("#cartaz-final").css({"width":size_px[0], "height":size_px[1]});
	$("#cartaz-final").attr("data-tamanho", size_cm[0]+"x"+size_cm[1]);
	$("#cartaz-final").attr("data-orientacao", orientacao);
	ajax_set_sheet_size(size_px[0]+"x"+size_px[1], size_cm[0]+"x"+size_cm[1]);
});

$(".orient-links").click(function(e){
	var z = $("#cartaz-final").attr('data-scala');
	var orientacao = $(this).attr("data-orient");
	var tamanho_cartaz_cm = $("#cartaz-final").attr("data-tamanho").split("x");

	var novo_tamanho_cartaz_cm = tamanho_cartaz_cm[1]+"x"+tamanho_cartaz_cm[0];

	var width = $("#cartaz-final").width();
	var height = $("#cartaz-final").height();
	var orientacao_cartaz_final = $("#cartaz-final").attr("data-orientacao");
	
	if (orientacao_cartaz_final != orientacao) {
		$("#cartaz-final").css({"width":height, "height":width});
		$("#cartaz-final").attr("data-tamanho", novo_tamanho_cartaz_cm);
		$("#folha").css({"width":height, "height":width});
		$("#cartaz-final").attr("data-orientacao",orientacao);
		$("#status-cartaz-orientacao").html(orientacao);
		$("#status-cartaz-zoom").html(z);
		ajax_set_sheet_size(height+'x'+width, novo_tamanho_cartaz_cm, orientacao);
	};	

});

$(".close").click(function(){
	$(this).parent().css({"display":"none"});
	$("#file-template").val('');
	$("#file-excel").val('');
});

$(".zoom-links").click(function(){
	var z = $(this).attr('data-zoom');
	if (z == 0) {
		$("#status-cartaz-zoom").html("Zomm: Normal");
		$("#cartaz-final").css('-webkit-transform','scale(1,1)');	
	} else {
		$("#status-cartaz-zoom").html("Zomm: -"+z);
		$("#cartaz-final").css('-webkit-transform','scale(0.'+z+',0.'+z+')');
	}
	$("#cartaz-final").css('transform-origin','left top');
	$("#cartaz-final").attr('data-scala',z);

	$.ajax({
		url: URL_RELATIVA+'dashboard/ajax_cartaz_update',  
		type: 'POST',
    	data: {zoom:z, id:$("#id_template").val()},
    	dataType: "text",
		success: function(result){
			console.log( result );
    }});	
});

$("#importar").click(function(){
	$("#formulario_de_importar_template").fadeIn('slow').css("display", "block");
	$("#formulario_de_importar_template form p").remove();
});

$("#fechar_formulario").click(function(){
	$("#formulario_de_importar_template").fadeOut('slow').css("display","none");	
	$("#formulario_de_importar_template form p").remove();
});

