<?php
/*
	Classe gerada pelo Build_Core 
	@author Maickon Rangel - maickon4developers@gmail.com
	Prodigio Framework - 2018
	Model: imagens
*/

class Imagens_Model extends Dbrecord_Core {

	private $permit;

	public function __construct(){
		parent::__construct();
		$this->permit = ['id','id_usuario','nome','caminho','created_at'];
	}

	public function get_permit(){
		return $this->permit;
	}
}