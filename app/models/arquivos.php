<?php
/*
	Classe gerada pelo Build_Core 
	@author Maickon Rangel - maickon4developers@gmail.com
	Prodigio Framework - 2018
	Model: arquivos
*/

include PATH_BASE.'extern_lib/excel/Classes/PHPExcel/IOFactory.php';
include PATH_BASE.'extern_lib/excel/PHPExcel.php';

class Arquivos_Model extends Dbrecord_Core {

	private $permit;

	public function __construct(){
		parent::__construct();
		$this->permit = ['id','id_usuario','nome','caminho','configuracao','created_at'];
	}

	public function get_permit(){
		return $this->permit;
	}

	function set_file($inputFileName){
        $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
        $this->table = $sheetData;
    }

    public function excel_array_format($array, $mode = 'all'){
  		$header = [];
  		$body = [];
  		$count_header  = 0;
  		foreach ($array[1] as $key => $value):
    			if (!empty($value)) {
    				$header[] = $value;
    				$count_header++;
    			} else {
    				break;
    			}
		endforeach;
		unset($array[1]);
		foreach ($array as $key => $value) {
			$count_body = 0;
			$each_body = [];
			foreach ($value as $key1 => $value1) {
				if ($count_body < $count_header) {
					$each_body[] = $value1;
				} else {
					break;
				}
				$count_body++;
			}
			$body[] = $each_body;
		}
		$products = [];
		if ($mode == 'header') {
			$products[] = $header;
		} elseif ($mode == 'body') {
			$products[] = $body;
		} else {
			$products = [$header, $body];
		}

		return $products;
  	}
}