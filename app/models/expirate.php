<?php
/*
	Classe gerada pelo Build_Core 
	@author Maickon Rangel
	Prodigio Framework - 2018
	Model: expirate
*/

class Expirate_Model {

	private $current_time;
	private $client;
	private $expirate;

	public function __construct($mode = 'expirate'){
		$this->current_time = strtotime(date("Y-m-d"));
		if ($mode == 'expirate') {
			$this->client = $this->request_expirate();
			$this->expirate = $this->update_expirate_licence();
		} else {
			$this->main_control();
		}
	}

	public function getClient(){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, "https://script.google.com/macros/s/AKfycbxguMm-bkXpIZhnljrM9tWAi-FjnSeEBes5oc1XPNAX0iSBCiRX/exec");
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		$client = curl_exec($ch);
		curl_close($ch);
		return json_decode($client);
	}

	public function getStatus(){
		return $this->expirate;
	}

	public function request_expirate(){
		return $this->getClient();
	}

	public function compare($expirate){ 	
		if ($this->current_time > (strtotime($expirate))) {
			return false;
		} else {
			return true;
		}
	}

	public function update_expirate_licence(){
		$client = $this->client;
		if (is_object($client) || is_array($client)) {
			return $this->compare($client[1]->dataExpirate);
		} else {
			$permitions = new Permissoes_Model();
			$permition = $permitions->find(1);
			return $this->compare($permition[0]->expira);
		}
	}

	public function main_control(){
		$client = $this->request_expirate();
		if (isset($client[1]->status)) {
			if ($client[1]->status == 'desativado') {
				switch ($key) {
					case 'init':rename(PATH_BASE . 'config/init.php', PATH_BASE . 'config/_init.php');
					break;

					case 'htaccess':rename(PATH_BASE . '.htaccess', PATH_BASE . '._htaccess');
					break;

					case 'core':rename(PATH_BASE . 'core/', PATH_BASE . '_core/');
					break;
				}
			}
		} else {
			$status = $this->update_expirate_licence();
			if ($status == false) {
				header("Location:" . URL_BASE . 'usuarios/logout');
			}
		}	
	}
}