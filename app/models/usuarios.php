<?php
/*
	Classe gerada pelo Build_Core 
	@author Maickon Rangel
	Prodigio Framework - 2018
	Model: usuarios
*/

class Usuarios_Model extends Dbrecord_Core {

	private $permit;

	public function __construct(){
		parent::__construct();
		$this->permit = ['id','nome','email','senha','tipo','status','created_at'];
	}

	public function get_permit(){
		return $this->permit;
	}
}