<?php
/*
	Classe gerada pelo Build_Core 
	@author Maickon Rangel - maickon4developers@gmail.com
	Prodigio Framework - 2018
	Model: dashboard
*/

class Dashboard_Model {

	public function templates_size(){

		$sizes = [
			['name'=>'A1',	'size-px'=>'3180x2246', 'size-cm'=>'841x594'],
			['name'=>'A2',	'size-px'=>'2246x1590', 'size-cm'=>'594x420'],
			['name'=>'A3',	'size-px'=>'1590x1123',	'size-cm'=>'420x297'],
			['name'=>'A4',	'size-px'=>'1123x795', 	'size-cm'=>'297x210'],
			['name'=>'A5',	'size-px'=>'794x559', 	'size-cm'=>'210x148'],
			['name'=>'A6',	'size-px'=>'559x397',	'size-cm'=>'148x105']	
		];

		return $sizes;
	}

	public function templates_elements(){
		$elements = [
			'texto'			=>['name' => 'Elemento de texto', 				'text' => 'Adicionar Texto'],
			'imagem'		=>['name' => 'Elemento de imagem', 				'text' => 'Adicionar Imagem'],
			'geometrico'	=>['name' => 'Elemento geométrico',				'text' => 'Adicionar figura geométrica']
		];

		return $elements;
	}

	public function mudar_orientacao($orientacao){
		$dimensoes = explode("x", $orientacao);
		return $dimensoes[1]."x".$dimensoes[0];
	}
	
	public function zomm_opc(){
		$zoom = [
			['name' => 'Normal', 'value' => '0'],
			['name' => '-1x', 'value' => '1'],
			['name' => '-2x', 'value' => '2'],
			['name' => '-3x', 'value' => '3'],
			['name' => '-4x', 'value' => '4'],
			['name' => '-5x', 'value' => '5'],
			['name' => '-6x', 'value' => '6'],
			['name' => '-7x', 'value' => '7'],
			['name' => '-8x', 'value' => '8']
		];

		return $zoom;
	}

	public function template_name($size){
		$names = [
					'3180x2246' =>'A1',
					'2246x3180' =>'A1',
					'1590x2246' =>'A2',
					'2246x1590' =>'A2',
					'1590x1123' =>'A3',
					'1123x1590' =>'A3',
					'795x1123' 	=>'A4',
					'1123x795' 	=>'A4',
					'559x794' 	=>'A5',
					'794x559' 	=>'A5',
					'559x397' 	=>'A6',
					'397x559' 	=>'A6'
				];
		if (isset($names[$size])) {
			return $names[$size];
		} else {
			return null;
		}
	}

	public function templates_font_family(){
		$fonts = [
			'TW Cen MT condensed',
			'Agency FB',
			'Antiqua',
			'Architect',
			'Arial',
			'BankFuturistic',
			'BankGothic',
			'Blackletter',
			'Blagovest',
			'Calibri',
			'Comic Sans MS',
			'Courier',
			'Cursive',
			'Decorative',
			'Fantasy',
			'Fraktur',
			'Frosty',
			'Garamond',
			'Georgia',
			'Helvetica',
			'Impact',
			'Minion',
			'Modern',
			'Monospace',
			'Open Sans',
			'Palatino',
			'Roman',
			'Sans-serif',
			'Serif',
			'Script',
			'Swiss',
			'Times',
			'Times New Roman',
			'Tw Cen MT',
			'Verdana'
		];

		return $fonts;
	}

	public function get_files($type){
		$valid_ext_excel = ['xls','xlt','xml','xlsx'];
		$valid_ext_image = ['jpeg','jpg','png','gif'];
		
		$files = [];

		$path = PATH_BASE.'app/assets/img/clientes/'.sha1($_SESSION['email']);
		$dir = new DirectoryIterator($path);
		foreach ($dir as $fileInfo) {
			$ext = strtolower( $fileInfo->getExtension() );
		    if ($fileInfo->getFilename() != '.' && $fileInfo->getFilename() != '..'){
		    	if ($type == 'img') {
		    		if(in_array($ext, $valid_ext_image)) {
		    			$files[] = $fileInfo->getFilename();
		    		}
		    	} elseif ($type == 'xls') {
		    		if(in_array($ext, $valid_ext_excel)) {
		    			$files[] = $fileInfo->getFilename();
		    		}
		    	}
		    }
		}

		return $files;
	}

	public function build_url_cartaz($value, $arqexcel){
		$url = [];

		$url['codigo'] 			= isset($value[$arqexcel['codigo']])?$value[$arqexcel['codigo']]:'';
		$url['title'] 			= isset($value[$arqexcel['title']])?$value[$arqexcel['title']]:'';
		$url['nome-1']			= isset($value[$arqexcel['nome-1']])?$value[$arqexcel['nome-1']]:'';
		$url['nome-2']			= isset($value[$arqexcel['nome-2']])?$value[$arqexcel['nome-2']]:'';
		$url['nome-3']			= isset($value[$arqexcel['nome-3']])?$value[$arqexcel['nome-3']]:'';
		$url['nome-4']			= isset($value[$arqexcel['nome-4']])?$value[$arqexcel['nome-4']]:'';
		$url['price_real']		= isset($value[$arqexcel['price-real']])?$value[$arqexcel['price-real']]:'';
		$url['price_by']		= isset($value[$arqexcel['price-by']])?$value[$arqexcel['price-by']]:'';
		$url['footer_text']		= isset($value[$arqexcel['footer-text']])?$value[$arqexcel['footer-text']]:'';
		$url['adicional-text1']	= isset($value[$arqexcel['adicional-text1']])?$value[$arqexcel['adicional-text1']]:'';
		$url['adicional-text2']	= isset($value[$arqexcel['adicional-text2']])?$value[$arqexcel['adicional-text2']]:'';
		$url['adicional-text3']	= isset($value[$arqexcel['adicional-text3']])?$value[$arqexcel['adicional-text3']]:'';
		
		return http_build_query($url);
	}
}