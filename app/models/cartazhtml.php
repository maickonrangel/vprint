<?php
/*
	Classe gerada pelo Build_Core 
	@author Maickon Rangel - maickon4developers@gmail.com
	Prodigio Framework - 2017
	Model: cartazes
*/

class Cartazhtml_Model extends Dbrecord_Core {
	private $permit;

	public function __construct(){
		parent::__construct();
		$this->permit = [
			'id',
			'id_usuario',
			'nome_cartaz',
			'tamanho_cartaz_px',
			'tamanho_cartaz_cm',
			'orientacao',
			'zoom',
			'cartaz_html',
			'created_at'
		];
	}

	public function get_permit(){
		return $this->permit;
	}

	public function to_style($style){
		return "style=\"{$style}\"";
	}

	public function to_style_rt($css){
		$parts = explode(";", $css);
		unset($parts[count($parts) - 1]);
		unset($parts[count($parts) - 1]);
		$styles = 'style="';
		foreach ($parts as $key => $value) {
			$styles .= "{$value};";
		}
		return "{$styles}\"";
	}

	public function get_text($text){
		$parts = explode(";", $text);
		return explode(':', $parts[count($parts) - 2])[1];
	}

	public function get_produtos(){
		$elementos = [];
		if (isset($_SESSION['produtos_cartaz'])) {
			$count = 0;
			$elem = "elem_{$count}";
			foreach ($_SESSION['produtos_cartaz'] as $key => $value) {
				parse_str($value, $out);
				foreach ($out as $outkey => $outvalue) {
					if ($outkey == 'price_real' and !empty($outvalue)) {
						if (strstr($outvalue, '.') == false) {
							$preco_por = explode(",", $outvalue);
						} else {
							$preco_por = explode(".", $outvalue);
						}

						if (isset($preco_por[0]) and $preco_por[1]) {
							$real = $preco_por[0];
							$cent = (strlen($preco_por[1]) == 1)?"{$preco_por[1]}0":substr($preco_por[1], 0, 2);
							$elementos[$key][] = $real;
							$elementos[$key][] = ','.$cent;
						}
					} elseif ($outkey == 'price_by' and !empty($outvalue)) {
						if (strstr($outvalue, '.') == false) {
							$preco_de = explode(",", $outvalue);
						} else {
							$preco_de = explode(".", $outvalue);
						}

						$preco_de = explode(".", $outvalue);
						if (isset($preco_de[0]) and $preco_de[1]) {
							$real = $preco_de[0];
							$cent = (strlen($preco_de[1]) == 1)?"{$preco_de[1]}0":substr($preco_de[1], 0, 2);
							$elementos[$key][] = $real;
							$elementos[$key][] = ','.$cent;
						}
					} else {
						$elementos[$key][] = $outvalue;
					}
					// if (!empty($outvalue)) {
					// }
				}
			}
		}
		return $elementos;
	}
}