<?php
/*
	Classe gerada pelo Build_Core 
	@author Maickon Rangel - maickon4developers@gmail.com
	Prodigio Framework - 2018
	Controller: dashboard
*/

class Dashboard_Controller extends Controller_Core {
	function __construct(){
		new Expirate_Model('control');
		$this->check_session();
		parent::__construct();
		// setanto os meta dados
		$this->meta_title = 'Dashboard - Vprint';
		$this->meta_description = 'Painel administrativo';
		$this->meta_keywords = 'Impressão, vprint, cartazes';

		$this->css_files = $this->set_base_css([
			'bootstrap.min',
			'dataTables.bootstrap4.min',
			'init',
			'dashboard',
			'spectrum',
			'jquery-ui',
			'folha']);
		$this->js_files = $this->set_base_js([
			'libs/jquery',
			'libs/popper',
			'libs/bootstrap.min',
			'libs/jquery-ui',
			'libs/feather.min',
			'dashboard-cartaz',
			'dblclick-elements',
			'change-elements',
			'click-elements',
			'draggable-elements',
			'resize-elements',
			'libs/spectrum',
			'libs/html2canvas.min',
			'libs/jspdf.min',
			'libs/zlib',
			'libs/png',
			'libs/addimage',
			'libs/png_support',
			'libs/jquery.dataTables.min',
			'libs/dataTables.bootstrap4.min']);
	}

	public function index(){
		$dashboard = new Dashboard_Model;
		$cartaz = new Cartazhtml_Model;

		$cartazes = $cartaz->__count('cartazes',"id_usuario={$_SESSION['id']}");

		$arquivo = new Arquivos_Model();
		$arquivos = $arquivo->__count('arquivos',"id_usuario={$_SESSION['id']}");

		$imagem = new Arquivos_Model();
		$imagens = $imagem->__count('imagens',"id_usuario={$_SESSION['id']}");
		
		require_once $this->render('index');
	}

	public function template($params = ''){
		global $_QUERY;
		$dashboard = new Dashboard_Model;
		
		$arquivo = new Arquivos_Model;
		$arquivo_atual = $arquivo->find_filter('*',"id_usuario = {$_SESSION['id']}");

		$cartaz = new Cartazhtml_Model;
		$cartaz_atual = $cartaz->find_filter('*',"id = {$params} and id_usuario = {$_SESSION['id']}");

		if (!empty($cartaz_atual[0]->tamanho_cartaz_px)) {
			$tamanho = explode("x", $cartaz_atual[0]->tamanho_cartaz_px);
			$width = $tamanho[0];
			$height = $tamanho[1];
		} else {
			$width = '800';
			$height = '1122';
		}
		
		$_SESSION['template-id'] = $params;

		$cartazes = $cartaz->find_filter('*',"id_usuario = {$_SESSION['id']}");

		$imagem = new Imagens_Model;
		$imagens = $imagem->find_filter('*',"id_usuario = {$_SESSION['id']}");

		require_once $this->render('template');

	}
	
	public function create_template(){
		global $_QUERY;
		$dashboard = new Dashboard_Model;
		$cartaz = new Cartazhtml_Model;
		$tamanhos = $dashboard->templates_size();
		$cartazes = $cartaz->find_by_column('*','id_usuario',$_SESSION['id']);
		require_once $this->render('criar-template');
	}

	public function save_template(){
		if (isset($_REQUEST)) {
			$cartaz = new Cartazhtml_Model();
			if ($cartaz->save()) {
				$this->redirect('create-template?status=success');
			} else {
				$this->redirect('create-template?status=error');
			}
		}
	}

	public function delete_template(){
		if (isset($_REQUEST) and count($_REQUEST) == 2) {
			$cartaz = new Cartazhtml_Model();
			if ($cartaz->delete($_REQUEST['id'])) {
				$this->redirect('create-template');
			} else {
				$this->redirect('create-template?status=error');
			}
		}	
	}

	public function ajax_select_html(){
		$cartaz_html = new Cartazhtml_Model;
		$cartaz = $cartaz_html->find($_REQUEST['id']);
		echo json_encode($cartaz[0]->cartaz_html);
	}

	public function ajax_get_text($type = ''){
		if ($_REQUEST and $type != '') {
			$text = '';
			if (isset($_SESSION['header_and_footer'][0])) {
				parse_str($_SESSION['header_and_footer'][0], $products);
				if ($type == 'header') {
					$text = $products['title'];
				} elseif ($type == 'footer') {
					$text = $products['footer_text'];
				}
			}
			echo $text;
		}
	}

	public function ajax_select_file(){
		if ($_REQUEST) {
			$arquivo = new Arquivos_Model;
			$arquivo_atual = $arquivo->find_filter('*',"id = {$_REQUEST['id']} and id_usuario = {$_SESSION['id']}");
			$arquivo->set_file(PATH_BASE.$arquivo_atual[0]->caminho);
			$table = $arquivo->excel_array_format($arquivo->table);
			echo json_encode($table[1]);
		} else {
			echo 'requisicao invalida!';
		}
	}

	public function ajax_select_config(){
		if ($_REQUEST) {
			$arquivo = new Arquivos_Model;
			$arquivo_atual = $arquivo->find_filter('*',"id = {$_REQUEST['id']} and id_usuario = {$_SESSION['id']}");
			$configuracao = $arquivo_atual[0]->configuracao;

			$arquivo->set_file(PATH_BASE.$arquivo_atual[0]->caminho);
			$table = $arquivo->excel_array_format($arquivo->table, 'body');

			$files_config['table'] = $table;
			$files_config['config'] = unserialize($configuracao);
			echo json_encode($files_config);
		} else {
			echo 'requisicao invalida!';
		}	
	}

	public function exportar_template($params = ''){
		$template = new Cartazhtml_Model;
		$file_name = "templates";
		$file = '';
		$insert_into = 'INSERT INTO `cartazhtml` (`id`, `id_usuario`, `nome_cartaz`, `tamanho_cartaz_px`, `tamanho_cartaz_cm`, `orientacao`, `zoom`, `cartaz_html`, `created_at`) VALUES
(';
		if ($params != '') {
			$file_name = "template-{$params}";
			$file = PATH_BASE."config/db/template-{$params}.sql";
        } else {
			$file = PATH_BASE."config/db/{$file_name}.sql";
        }

		if (file_exists($file)) {
			unlink($file);
		}

		$sql = "
				SELECT * INTO OUTFILE '{$file}'
              	FIELDS 
              		TERMINATED BY ',' 
              		OPTIONALLY ENCLOSED BY '\"'
              	LINES 
              		STARTING BY '{$insert_into}'
              		TERMINATED BY ');'
              	FROM cartazhtml ";

        if ($params != '') {
        	$sql .= "WHERE id = {$params};";
        } else {
        	$sql .= ";";
        }

		$result = $template->__sql($sql);
		header("Location: ".URL_BASE."config/db/{$file_name}.sql");
	}

	public function importar_template(){
		$valid_ext = ['sql'];
		$ext = strtolower(pathinfo($_FILES['template']['name'], PATHINFO_EXTENSION));
		if (in_array($ext, $valid_ext)) {
			$template = new Cartazhtml_Model;
			$insert_cartaz_estilos = file_get_contents($_FILES['template']['tmp_name']);
			if ($template->__sql($insert_cartaz_estilos) == 0) {
				header("Location: /vprint/list-templates?error=import");
			} else {
				header("Location: /vprint/list-templates?success=import");
			}		
		} else {
			header("Location: /vprint/list-templates?error=ext");
		}
	}

	public function ajax_cartaz_update(){
		$cartaz = new Cartazhtml_Model();
		if ($cartaz->update()) {
			echo 'sucesso update';
		} else {
			echo 'erro update';
		}	
	}

	public function ajax_set_size(){
		$cartaz = new Cartazhtml_Model();
		
		if ($cartaz->update()) {
			echo 'sucesso update';
		} else {
			echo 'erro update';
		}	
	}

	public function ajax_add_products(){
		if ($_REQUEST) {
			if (isset($_REQUEST['qtd']) and $_REQUEST['qtd'] > 1) {
				for ($i=0; $i < $_REQUEST['qtd']; $i++) {
					$_SESSION['produtos_cartaz'][] = $_REQUEST['data_url'];
				}
			} else {
				$_SESSION['produtos_cartaz'][$_REQUEST['data_id']] = $_REQUEST['data_url'];
			}
			print_r($_SESSION['produtos_cartaz']);
		} else {
			echo 'requisicao invalida';
			print_r($_SESSION['produtos_cartaz']);
		}
	}

	public function ajax_remove_products(){
		if ($_REQUEST) {
			echo "desmarcado\n";
			$id = $_REQUEST['data_id'];
			unset($_SESSION['produtos_cartaz'][$id]);
			print_r($_SESSION['produtos_cartaz']);
		} else {
			echo 'requisicao invalida';
			print_r($_SESSION['produtos_cartaz']);
		}
	}

	public function ajax_clear_products(){
		unset($_SESSION['produtos_cartaz']);
	}

	public function meus_dados(){
		$dashboard = new Dashboard_Model();
		$usuario = new Usuarios_Model();
		$dados = $usuario->find_by_column('id,nome,email,created_at','id',$_SESSION['id']);
		require_once $this->render('meus-dados');
	}

	public function visualizar_arquivo($params = ''){
		if (empty($params)) {
			$this->error('Esta página não pode ser encontrada');
		} else if (is_array($params)) {
			$dashboard = new Dashboard_Model();
			$arquivo = new Arquivos_Model();
			$arquivo_excel = $arquivo->find_by_column('caminho, configuracao, id, nome','id',$params[0]);
			if ($params[1] == 'novo') {
				unset($_SESSION['produtos_cartaz']);
			}
			if (empty($arquivo_excel[0]->id)) {
				$this->error('Esta página não pode ser encontrada');
			} else {
				$_SESSION['tabela_excel_selecionada'] = $params[0];
				$arquivo->set_file(PATH_BASE.$arquivo_excel[0]->caminho);
				$table = $arquivo->excel_array_format($arquivo->table);
				require_once $this->render('visualizar-arquivo');
			}
		} else {
			$this->error('Esta página não pode ser encontrada');
		}
	}

	public function visualizar_tabela(){
		if (isset($_SESSION['tabela_excel_selecionada'])) {
			$dashboard = new Dashboard_Model();
			$arquivo = new Arquivos_Model();
			$arquivo_excel = $arquivo->find_by_column('caminho, configuracao, id','id',$_SESSION['tabela_excel_selecionada']);

			$arquivo->set_file(PATH_BASE.$arquivo_excel[0]->caminho);
			$table = $arquivo->excel_array_format($arquivo->table);
			require_once $this->render('visualizar-arquivo');
		} else {
			$this->redirect('importar-arquivo');
		}
	}

	public function ajax_get_files(){
		if ($_REQUEST) {
			$dashboard = new Dashboard_Model();
			$files = $dashboard->get_files($_REQUEST['type']);
			echo json_encode($files);
		}
	}

	public function atualizar_arquivos(){
		global $_QUERY;
		if ($_REQUEST and count($_REQUEST) == 4) {
			$valid_ext = ['xls','xlt','xml','xlsx','jpeg','jpg','png','gif'];
			$ext = strtolower(pathinfo($_FILES['arquivo']['name'],PATHINFO_EXTENSION));
			if (in_array($ext, $valid_ext)) {
				$path = 'app/assets/img/clientes/'.sha1($_SESSION['email']).'/'.$_REQUEST['file_list'];
				if(move_uploaded_file($_FILES['arquivo']['tmp_name'], PATH_BASE.$path)){
					$this->redirect('atualizar-arquivo?status=update-success');
				} else {
					$this->redirect('atualizar-arquivo?status=update-error');
				}
			} else {
				$this->redirect('atualizar-arquivo?status=extension-error');
			}
		}
		$dashboard = new Dashboard_Model();
		require_once $this->render('atualizar-arquivo');
	}

	public function importar_arquivos(){
		global $_QUERY;
		$dashboard = new Dashboard_Model();
		$arquivo = new Arquivos_Model();
		$arquivos = $arquivo->find_by_column('*','id_usuario',$_SESSION['id']);
		if (empty($arquivos[0]->id)) {
			$arquivos = [];
		}
		require_once $this->render('importar-arquivos');
	}

	public function salvar_arquivo(){
		$valid_ext = ['xls','xlt','xml','xlsx'];
		$_REQUEST['arquivo_excel'] = $_FILES["arquivo_excel"];
		$arquivo = new Arquivos_Model();
		$arquivos = $arquivo->find_by_column('nome','nome',$_REQUEST['nome']);
		if (empty($arquivos[0]->id)) {
			$ext = strtolower(pathinfo($_FILES['arquivo_excel']['name'],PATHINFO_EXTENSION));
			if (in_array($ext, $valid_ext)) {
				$path = 'app/assets/img/clientes/'.sha1($_SESSION['email']).'/'.$_REQUEST['nome'].'.'.$ext;
				if(move_uploaded_file($_REQUEST['arquivo_excel']['tmp_name'], PATH_BASE.$path)){
					$arquivo = new Arquivos_Model();
					unset($_REQUEST['arquivo_excel']);
					$_REQUEST['caminho'] = $path;
					if ($arquivo->save()) {
						$this->redirect('importar-arquivo?status=saved');
					}
				} else {
					$this->redirect('importar-arquivo?status=upload-error');
				}
			} else {
				$this->redirect('importar-arquivo?status=extension-error');
			}
		} else {
			$this->redirect('importar-arquivo?status=duplicate');
		}
	}

	public function salvar_configuracao(){
		if (isset($_REQUEST)) {
			$id = $_REQUEST['id'];
			unset($_REQUEST['id']);
			unset($_REQUEST['url']);
			$configuracao = serialize($_REQUEST);
			unset($_REQUEST);
			$_REQUEST['configuracao'] = $configuracao;
			$_REQUEST['id'] = $id;
			$arquivo = new Arquivos_Model();
			if ($arquivo->update()) {
				$this->redirect('dashboard/configurar_arquivo/cabecalho/'.$id.'/?status=success');
			} else {
				$this->redirect('dashboard/configurar_arquivo/cabecalho/'.$id.'/?status=error');
			}
		}
	}

	public function delete_arquivo(){
		if (isset($_REQUEST) and count($_REQUEST) == 2) {
			$arq = new Arquivos_Model();
			$arquivo = $arq->find_by_column('*','id',$_REQUEST['id']);
			if ($arq->delete($_REQUEST['id'])) {
				$path = 'app/assets/img/clientes/'.sha1($_SESSION['email']).'/'.$arquivo[0]->nome;
				if (file_exists(PATH_BASE.$arquivo[0]->caminho)) {
					unlink(PATH_BASE.$arquivo[0]->caminho);	
				}
				$this->redirect('importar-arquivo?status=success-delete');
			} else {
				$this->redirect('importar-arquivo?status=error-delete');
			}
		}	
	}

	public function configurar_arquivo($params = ''){
		global $_QUERY;
		$dashboard = new Dashboard_Model();
		$arquivo = new Arquivos_Model();
		$arquivos = $arquivo->find_filter('*',"id_usuario = {$_SESSION['id']}");
		if (is_array($params)) {
			$arquivo_formulario = $arquivo->find_filter('*',"id = {$params[1]} and id_usuario = {$_SESSION['id']}");
		}
		$table = null;
		if (isset($params[0]) and $params[0] == 'cabecalho') {
			$arquivo_excel = $arquivo->find_by_column('*','id',$params[1]);
			$arquivo->set_file(PATH_BASE.$arquivo_excel[0]->caminho);
			$table = $arquivo->excel_array_format($arquivo->table, 'header');
		}
		require_once $this->render('configurar-arquivo');
	}

	public function list_templates($params = ''){
		global $_QUERY;
		$dashboard = new Dashboard_Model;
		$cartaz = new Cartazhtml_Model;
		$cartazes = $cartaz->find_filter('*',"id_usuario = {$_SESSION['id']}");
		require_once $this->render('listar-templates');	
	}

	public function upload_image(){
		global $_QUERY;
		$dashboard = new Dashboard_Model;
		$imagem = new Imagens_Model();
		$imagens = $imagem->find_filter('*',"id_usuario = {$_SESSION['id']}");
		require_once $this->render('enviar-imagens');
	}

	public function save_image(){
		$valid_ext = ['jpeg','jpg','png','gif'];
		$_REQUEST['imagens'] = $_FILES["imagens"];
		$imagem = new Imagens_Model();
		$imagens = $imagem->find_by_column('nome','nome',$_REQUEST['nome']);
		$count = count($_REQUEST['imagens']['name']);
		$nome = $_REQUEST['nome'];
		$_QUERY['msg'] = '';
		if (empty($imagens[0]->id)) {
			for ($key = 0; $key < $count; $key++) {
				$copy = $key+1;
				$ext = strtolower(pathinfo($_FILES['imagens']['name'][$key],PATHINFO_EXTENSION));
				$_REQUEST['nome'] = $nome.'-v.'.$copy; 
				if (in_array($ext, $valid_ext)) {					
					$path = 'app/assets/img/clientes/'.sha1($_SESSION['email']).'/'.$_REQUEST['nome'].'.'.$ext;
					if(move_uploaded_file($_FILES['imagens']['tmp_name'][$key], PATH_BASE.$path)){
						$imagem = new Imagens_Model();
						unset($_REQUEST['imagens']);
						$_REQUEST['caminho'] = $path;
						if ($imagem->save()) {
							$_QUERY['msg'] .= "Arquivo {$_REQUEST['nome']} salvo com sucesso!<br>";
						} else {
							$_QUERY['msg'] .= "Arquivo {$_REQUEST['nome']} não pode ser salvo!<br>";
						}
					} else {
						$_QUERY['msg'] .= "Cópia do arquivo {$_REQUEST['nome']} não foi criada com sucesso!<br>";
					}
				} else {
					$_QUERY['msg'] .= "A extenssão do arquivo {$_REQUEST['nome']} não é válida.<br>";
				}
			}
			$this->redirect('enviar-imagens?msg='.$_QUERY['msg']);
		} else {
			$this->redirect('enviar-imagens?status=duplicate');
		}
	}

	public function delete_image(){
		if (isset($_REQUEST) and count($_REQUEST) == 2) {
			$img = new Imagens_Model();
			$imagem = $img->find_by_column('*','id',$_REQUEST['id']);
			if ($img->delete($_REQUEST['id'])) {
				$path = 'app/assets/img/clientes/'.sha1($_SESSION['email']).'/'.$imagem[0]->nome;
				if (file_exists(PATH_BASE.$imagem[0]->caminho)) {
					unlink(PATH_BASE.$imagem[0]->caminho);	
					$this->redirect('enviar-imagens?status=success-delete');
				} else {
					$this->redirect('enviar-imagens?status=error-delete-file');
				}
			} else {
				$this->redirect('enviar-imagens?status=error-delete');
			}
		}	
	}

	public function videos(){
		$dashboard = new Dashboard_Model();
		require_once $this->render('videos');	
	}

	public function sobre(){
		$dashboard = new Dashboard_Model();
		require_once $this->render('sobre');	
	}
}