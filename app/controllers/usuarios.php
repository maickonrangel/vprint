<?php
/*
	Classe gerada pelo Build_Core 
	@author Maickon Rangel - maickon4developers@gmail.com
	Prodigio Framework - 2018
	Controller: usuarios
*/

class Usuarios_Controller extends Controller_Core {
	function __construct(){
		parent::__construct();
		// setanto os meta dados
		$this->meta_title = 'Usuários';
		$this->meta_description = 'Usuários';
		$this->meta_keywords = 'Usuários';

		// [Voce pode passar arquivos css para a pagina do seu controller apenas 
		// informando o array como parametro de $this->set_base_css()]

		// chamando css em assets/css
		$this->css_files = $this->set_base_css(['bootstrap.min','signin']);
		
		// chamando css interno dentro da view e concatenando ao css_files
		// $this->css_files .= $this->set_css(['index','home']);
		
		// [Voce pode passar arquivos javascript para ser chamado na view deste  
		// controller apenas passando um array com os nomes dos arquivos sem a 
		// extençao no array em $this->set_base_js]

		// chamada de arquivos js dentro de assets
		$this->js_files = $this->set_base_js(['libs/jquery']);
		// chamada de arquivos js dentro da veiw 
		// $this->js_files .= $this->set_js(['index','teste']);
	}

	public function index(){
		require_once $this->render('index');
	}

	public function nova_conta(){
		global $_QUERY;
		require_once $this->render('nova_conta');
	}

	public function criar_conta(){
		if (isset($_REQUEST) and count($_REQUEST) == 4) {
			$hash = password_hash($_REQUEST['senha'], PASSWORD_DEFAULT);
			$_REQUEST['senha'] = $hash;
			$user = new Usuarios_Model;
			$users = $user->find_all();
			$exists = 0;
			foreach ($users as $key => $value) {
				if ($_REQUEST['email'] == $value->email && password_verify($_REQUEST['senha'], $hash) && $_REQUEST['nome'] == $value->nome) {
					$_REQUEST['id'] = $value->id;
					$exists = 1;
				}
			}
			if ($exists == 1) {
				$this->redirect('nova-conta?status=user-exists');
			} else {
				if($user->save()){
					if (!file_exists(PATH_BASE.'app/assets/img/clientes/'.sha1($_REQUEST['email']))) {
						mkdir(PATH_BASE.'app/assets/img/clientes/'.sha1($_REQUEST['email']));
					}
					@session_start();
					$_SESSION['login'] 	= explode(' ',$_REQUEST['nome'])[0];
					$_SESSION['nome'] 	= $_REQUEST['nome'];
					$_SESSION['email'] 	= $_REQUEST['email'];
					$_SESSION['id'] 	= $_REQUEST['id'];
					$this->redirect('dashboard');
				} 
			}
		}
	}

	public function recuperar_conta(){
		global $_QUERY;
		$user = new Usuarios_Model;
		$users = $user->find_all();
		require_once $this->render('recuperar_conta');
	}

	public function atualizar_conta(){
		if (isset($_REQUEST)) {
			$hash = password_hash($_REQUEST['senha'], PASSWORD_DEFAULT);
			$_REQUEST['senha'] = $hash;
			$user = new Usuarios_Model;
			if ($user->update()) {
				$this->redirect('init??status=update-success');
			} else {
				$this->redirect('init??status=update-error');
			}
		}
		print_r($_REQUEST);
	}

	public function logar(){
		$expirate = new Expirate_Model();
		if (isset($_REQUEST) and count($_REQUEST) == 3 and $expirate->getStatus() == true) {
			$user = new Usuarios_Model;
			$users = $user->find_all();
			$exists = 0;
			
			foreach ($users as $key => $value) {
				if (($_REQUEST['email'] == $value->email) && (password_verify($_REQUEST['senha'], $value->senha))) {
					@session_start();
					$_SESSION['login'] 	= explode(' ',$value->nome)[0];
					$_SESSION['nome'] 	= $value->nome;
					$_SESSION['email'] 	= $value->email;
					$_SESSION['tipo'] 	= $value->tipo;
					$_SESSION['id'] 	= $value->id;
					$exists = 1;
				}
			}
			if ($exists == 1) {
				$this->redirect('dashboard');
			} else {
				$this->redirect('init?status=user-not-find');
			}
		} else {
			$this->redirect('init?status=internal-error');
		}
	}

	public function logout(){
		@session_start();
		session_destroy();
		$this->redirect();
	}	
}