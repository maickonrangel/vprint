<?php
/*
	Classe gerada pelo Build_Core 
	@author Maickon Rangel - maickon4developers@gmail.com
	Prodigio Framework - 2018
	Controller: cartazhtml
*/

class Cartazhtml_Controller extends Controller_Core {
	function __construct(){
		$this->check_session();
		parent::__construct();
		// setanto os meta dados
		$this->meta_title = 'Dashboard - Vprint';
		$this->meta_description = 'Painel administrativo';
		$this->meta_keywords = 'Impressão, vprint, cartazes';

		$this->css_files = $this->set_base_css([
			'bootstrap.min',
			'dataTables.bootstrap4.min',
			'init',
			'dashboard',
			'spectrum',
			'jquery-ui',
			'folha']);
		$this->js_files = $this->set_base_js([
			'libs/jquery',
			'libs/popper',
			'libs/bootstrap.min',
			'libs/jquery-ui',
			'libs/feather.min',
			'dashboard-cartaz',
			'dblclick-elements',
			'change-elements',
			'click-elements',
			'draggable-elements',
			'resize-elements',
			'libs/spectrum',
			'libs/html2canvas.min',
			'libs/jspdf.min',
			'libs/zlib',
			'libs/png',
			'libs/addimage',
			'libs/png_support',
			'libs/jquery.dataTables.min',
			'libs/dataTables.bootstrap4.min']);
	}

	public function index(){
		$dashboard = new Dashboard_Model;
		$cartaz = new Cartazes_Model;

		$cartazes = $cartaz->__count('cartazes',"id_usuario={$_SESSION['id']}");

		$arquivo = new Arquivos_Model();
		$arquivos = $arquivo->__count('arquivos',"id_usuario={$_SESSION['id']}");

		$imagem = new Arquivos_Model();
		$imagens = $imagem->__count('imagens',"id_usuario={$_SESSION['id']}");
		
		require_once $this->render('index');
	}

	public function ajax_save_cartaz_html(){
		if ($_REQUEST) {
			$cartaz_html = new Cartazhtml_Model;
			$cartaz = $cartaz_html->find($_REQUEST['id']);
			if (!empty($cartaz[0]->id)) {
				$_REQUEST['id_usuario'] = $_SESSION['id'];
				if ($cartaz_html->update()){
					echo 'cartaz atualizado';
				} else {
					echo 'cartaz não atualizado';
				}
			} else {
				echo 'erro ao atualizar cartaz. Cartaz não existe.';
			}
		}
	}

	public function ajax_get_produtos(){
		$cartaz = new Cartazhtml_Model;
		$produtos = $cartaz->get_produtos();
		if (isset($_SESSION['cartaz_modo_exibir'])) {
			$modo = $_SESSION['cartaz_modo_exibir'];
		} else {
			$modo = 'muitos';
		}
		$cartaz_produtos = ['modo' => $modo, 'produtos' => $produtos];
		echo json_encode($cartaz_produtos);
	}

	public function ajax_set_modo_cartaz(){
		if(isset($_REQUEST['modo'])) {
			$_SESSION['cartaz_modo_exibir'] = $_REQUEST['modo'];
		} else {
			$_SESSION['cartaz_modo_exibir'] = 'muitos';
		}
	}
}