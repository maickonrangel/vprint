<?php
/*
	Classe gerada pelo Build_Core 
	@author Maickon Rangel - maickon4developers@gmail.com
	Prodigio Framework - 2017
	Controller: init
*/

class Init_Controller extends Controller_Core {
	function __construct(){
		parent::__construct();
		// setanto os meta dados
		$this->meta_title = 'Vprint';
		$this->meta_description = 'Criação de templates para cartazes online.';
		$this->meta_keywords = 'Cartaze';

		// [Voce pode passar arquivos css para a pagina do seu controller apenas 
		// informando o array como parametro de $this->set_base_css()]

		// chamando css em assets/css
		$this->css_files = $this->set_base_css(['bootstrap.min','signin']);
		
		// chamando css interno dentro da view e concatenando ao css_files
		// $this->css_files .= $this->set_css(['index','home']);
		
		// [Voce pode passar arquivos javascript para ser chamado na view deste  
		// controller apenas passando um array com os nomes dos arquivos sem a 
		// extençao no array em $this->set_base_js]

		// chamada de arquivos js dentro de assets
		$this->js_files = $this->set_base_js(['libs/jquery']);
		// chamada de arquivos js dentro da veiw 
		// $this->js_files .= $this->set_js(['index','teste']);
		$this->version = 'Versão 0.7';
	}

	public function index(){
		$init = new Init_Model;
		if ($init->getConn() == false) {
			$this->redirect('configurar-db');
		} else {
			global $_QUERY;
			if (isset($_SESSION) and isset($_SESSION['login']) and isset($_SESSION['id'])) {
				$this->redirect('dashboard');
			} else {
				require_once $this->render('index');
			}
		}
	}

	public function config_db(){
		global $_QUERY;
		require_once $this->render('config-db');
	}

	public function create_db(){
		if ($_REQUEST) {
			$init = new Init_Model;
			$init->setConn('localhost', 'root', '');
			$table_usuarios = [
				'table_name'	=>'usuarios',
					'nome'		=>'varchar(255)',
					'email'		=>'varchar(255)',
					'senha'		=>'varchar(255)',
					'tipo'		=>'varchar(255) DEFAULT \'user\'',
					'status'	=>'integer DEFAULT 1'
			];

    		$table_imagens = [
				'table_name'	=>'imagens',
					'id_usuario'=>'integer',
					'nome'		=>'varchar(255)',
					'caminho'	=>'varchar(255)'
			];

			$table_cartazhtml = [
				'table_name'		=>'cartazhtml',
					'id_usuario'		=>'integer',
					'nome_cartaz' 		=> 'varchar(255)',
					'tamanho_cartaz_px' => 'varchar(255)',
					'tamanho_cartaz_cm' => 'varchar(255)',
					'orientacao' 		=> 'varchar(255)',
					'zoom' 				=> 'varchar(255)',
					'cartaz_html'		=>'longtext'
			];

    		$table_arquivos = [
				'table_name'		=>'arquivos',
					'id_usuario'	=>'integer',
					'nome'			=>'varchar(255)',
					'caminho'		=>'varchar(255)',
					'configuracao'	=>'text'
			];

			$table_permissoes = [
				'table_name'		=>'permissoes',
					'id_usuario'	=>'integer',
					'nome'			=>'varchar(255)',
					'expira'		=>'date'
			];
    		
    		$senha_adm = password_hash("admin123", PASSWORD_DEFAULT);
    		$senha_user = password_hash("usuario123", PASSWORD_DEFAULT);
    		$insert_cartaz_estilos1 = file_get_contents(PATH_BASE.'config/db/template1.sql');
    		$insert_cartaz_estilos2 = file_get_contents(PATH_BASE.'config/db/template2.sql');
    		$insert_cartaz_estilos3 = file_get_contents(PATH_BASE.'config/db/template3.sql');
    		$insert_cartaz_estilos4 = file_get_contents(PATH_BASE.'config/db/template4.sql');
    		$insert_cartaz_estilos5 = file_get_contents(PATH_BASE.'config/db/template5.sql');
    		
    		$init->__sql("DROP DATABASE IF EXISTS vprint_db;");
    		$init->__sql("DELETE FROM mysql.user WHERE user='vprint' and host='localhost'; FLUSH PRIVILEGES;");

    		if ( $init->__createDatabase($_REQUEST['db_name']) &&
					$init->__createUser($_REQUEST['db_user'], $_REQUEST['db_host'], $_REQUEST['db_pass']) &&
					$init->__createPrivuleges($_REQUEST['db_user'], $_REQUEST['db_host']) &&
    				$init->__createTable($table_usuarios) &&
    				$init->__createTable($table_imagens) &&
    				$init->__createTable($table_cartazhtml) &&
    				$init->__createTable($table_arquivos) &&
    				$init->__createTable($table_permissoes) &&
     				$init->__fkReferences('arquivos', 'id_usuario', 'usuarios') &&
    				$init->__fkReferences('cartazhtml', 'id_usuario', 'usuarios') &&
    				$init->__sql("INSERT INTO usuarios (nome,email,senha,tipo) VALUES('admin','admin@email.com','".$senha_adm."','admin')") &&
    				$init->__sql("INSERT INTO usuarios (nome,email,senha,tipo) VALUES('usuario','usuario@email.com','".$senha_user."','user')") &&
    				$init->__sql($insert_cartaz_estilos1) &&
    				$init->__sql($insert_cartaz_estilos2) &&
    				$init->__sql($insert_cartaz_estilos3) &&
    				$init->__sql($insert_cartaz_estilos4) &&
    				$init->__sql($insert_cartaz_estilos5) &&
    				$init->__sql("INSERT INTO permissoes (id_usuario,nome,expira) VALUES(1,'Supermercados Super Bom','2018-24-09')")
    				){

    			$file_config = '<?xml version="1.0" encoding="UTF-8"?>'."\n<config>\n";
				$file_config .="\t<db_name>{$_REQUEST['db_name']}</db_name>\n";
				$file_config .="\t<db_host>{$_REQUEST['db_host']}</db_host>\n";
				$file_config .="\t<db_user>{$_REQUEST['db_user']}</db_user>\n";
				$file_config .="\t<db_pass>{$_REQUEST['db_pass']}</db_pass>\n";
				$file_config .= '</config>';

				$uri = explode('/', $_SERVER['REQUEST_URI']);
				$path = $_SERVER['DOCUMENT_ROOT'].'/'.$uri[1];
				
				if(file_put_contents($path.'/config/setup-db.xml', $file_config)){
    				$this->redirect();
				}

    		} else {
    			$this->redirect('configuracao?status=error-db');
    		}
		}
	}
}