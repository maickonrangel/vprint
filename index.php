<?php

function getClient(){
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_URL, "https://script.google.com/macros/s/AKfycbxguMm-bkXpIZhnljrM9tWAi-FjnSeEBes5oc1XPNAX0iSBCiRX/exec");
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	$client = curl_exec($ch);
	curl_close($ch);
	return json_decode($client);
}

if (file_exists('config/init.php') && file_exists('core/') && file_exists('.htaccess')) {
	if (file_exists('config/db/config.inc.php')) {
		if (copy('config/db/config.inc.php', '../../phpMyAdmin/config.inc.php')){
			rename('config/db/config.inc.php', 'config/db/configured.inc.php');
		}
	}
	require 'config/init.php';
	new Routes_Core;
} else {
	$client = getClient();
	if (isset($client[1]->status)) {
		if ($client[1]->status == 'ativo') {
			switch ($key) {
				case 'init':
					(file_exists('config/_init.php'))?rename('config/_init.php', 'config/init.php'):'';
				break;

				case 'htaccess':
					file_exists('._htaccess')?rename('._htaccess', '.htaccess'):'';
				break;

				case 'core':
					file_exists('_core')?rename('_core', 'core'):'';
				break;
			}
		}
	}
	echo '
	<div style="width:100%;height:100%;background: linear-gradient(to bottom, #6699ff 0%, #0036D9 100%);">
		<div style="width:70%;height:auto;text-align:center;margin:0 auto;padding-top:12%;color:#FFF;">
			<h1 style="font-size:60px!important;">
			Erro na inicialização do Vprint.
			</h1>
			<p style="font-size:25px!important;">
				Tente atualizar a página ou entre em contato com o suporte do sistema.
			</p>
			<br>
			<style>a:hover{border:2px solid #000;}</style>
			<a href="#" onclick="location.reload();" style="background:#FFF;padding:20px;color:#000;text-decoration:none;font-weight:bold;font-size:18px;">Atualizar página</a>
		</div>
	</div>';
}

